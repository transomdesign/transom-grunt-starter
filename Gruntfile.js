module.exports = function(grunt) {
  var globalConfig = {
    src: "_assets", // Where are your uncompiled source files
    dest: "public_html/assets", // Where should those files end up?
    dest_stylesheets: "stylesheets", // Folder Name
    dest_javascripts: "javascripts", // Folder Name
    dest_images: "images", // Folder Name
    dest_javascripts_filename: "application", // File Name
    browserify_src: "<%= globalConfig.src %>/javascripts/init.js",
    browserify_dest: "<%= globalConfig.dest %>/<%= globalConfig.dest_javascripts %>/<%= globalConfig.dest_javascripts_filename %>.js", // File Name
  };

  var rsync = grunt.file.readJSON('Grunt.rsync.craft.json');
  var localConfig = grunt.file.readJSON('Grunt.config.local.json');

  require('time-grunt')(grunt);
  require('load-grunt-config')(grunt, {
    jitGrunt: true,
    config: {
      globalConfig: globalConfig,
      localConfig: localConfig,
      rsync: rsync
    }
  });

};
