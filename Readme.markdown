##Overview
1. File Structure
2. Grunt Overview
3. SASS Overview
4. JavaScript Overview
5. Deployments

##Getting Started

**IMPORTANT:**  
If you have just cloned this project for the first time, run `npm run init`.

This command will install all of your npm & bower dependencies and creates and will also create `Grunt.config.local.json` and `_config/local` which is where your local Grunt & Craft config config values are stored.

Behind the scenes we are creating copies of:  
`Grunt.config.local.json.sample`, and `_config/local-copy-me`   

...then running:  
`npm update --save-dev` 
`npm update --save` 
`bower install --save`

`npm update --save & --save-dev` applies version numbers to any package that has an `*` for its version. If there are version already set for packages, it will just install the packages to your project. 

##1. File Structure

###`_assets` vs `public_html/assets`

All of our Grunt files, Bower Components, and un-compiled SASS & JS Partials aren't used on live websites. We find it best practices to keep those files separate from CMS files & template directories. The `_assets` folder in the root of this git project is where we put all of our uncompiled images, javascript, SASS, and icons. Anything that is a static asset in our projects is stored here and processed with grunt. Even if that processing is just a simple copy task moving fonts from `_assets` to `public_html/assets`

It is best practice for Git to ignore the compiled assets inside of `public_html/assets`. 

##2. Grunt 

The primary use for Grunt is to build & optomize our SASS, JS & Images. 

[Read this article](http://mattbailey.io/a-beginners-guide-to-grunt-redux/) to understand the background behind our Grunt setup.

####Config
We use [load-grunt-config](https://github.com/firstandthird/load-grunt-config) to break up our config files into smaller, easier to read files.

These config files are located in the `/grunt`folder and named after their grunt tasks. I.E. `grunt sass` would be located in `/grunt/sass.js`

`grunt/alias.yaml` is where you define bigger tasks built from smaller taks. 

**Custom Global & Local Config**  
At the top of `Gruntfile.js` is an object named `globalConfig`. These config variables are mainly used to set the paths & filenames of our assets source and destinations. The later variables are useful for configuring deployment variables. 

There is also a config object named `localConfig` loads `Grunt.config.local.json`. Since local environments can vary between computers, we keep this file hidden from Git. 

####Common Grunt Tasks
**`grunt`** This default task builds the project and initiates the `watch` tasks. You'll be using this one the most.

**`grunt build`** Clears out the old compiled folder and then re-build all of your SASS, JS, Images, etc. 

**`grunt buildSASS`** 
Initiated when `watch` detects changes in your SASS files. 

**`grunt buildJS`**  Initiated when `watch` detects changes in your JS files. 

####Concurrent
We can execute some of our grunt tasks at the same time with this plugin. [This article](http://mattbailey.io/a-beginners-guide-to-grunt-redux/) outlines the concept some. Or [view the main repo](https://github.com/sindresorhus/grunt-concurrent)

####BrowserSync
We use Browsersync to automatically reload your page when JS, CSS or Templates change. 

Learn more at [www.browsersync.io](https://www.browsersync.io/)

####Javascript & Browserify
[Grunt Browserify](https://github.com/jmreidy/grunt-browserify) to manage dependencies & compile our Javascript files. 

Browserify lets you `require('modules')` in the browser by bundling up all of your dependencies. You can install & `require(library-name)` libraries by running `npm install <library-name> --save`. See `_assets/javascripts/init.js` for examples of this module loading. 


####CSS, SASS & Autoprefixer
We compile our SASS files with [`grunt-sass`](https://github.com/sindresorhus/grunt-sass). After compiling, we run our CSS though [`grunt-autoprefixer`](https://github.com/nDmitry/grunt-autoprefixer) which automatically adds vendor prefixes to our project. 

####Icon Sysetms with SVGStore
We use SVG Sprites for our icon systems. See the methods outlined in [this article](http://css-tricks.com/svg-sprites-use-better-icon-fonts/).

The SVG Sprite is then loaded in `_assets/javascripts/init.js` using the methods [outlined here](https://css-tricks.com/ajaxing-svg-sprite/)

####Imagemin
Reduces the size of your `_assets/images` and then copies them into your destination folder.
[Read more here](https://github.com/gruntjs/grunt-contrib-imagemin) 


## 3. Sass Starter Files


####1. Overview

We like to use a bunch of partial SCSS files. `application.scss` combines all of these files. Source order matters. 

Our sass usually falls into the following categories. These files also need to be imported in this order. Each category builds off of the previous one. 

[Read more about the BEMIT methodologies we use.](https://csswizardry.com/2015/08/bemit-taking-the-bem-naming-convention-a-step-further/)


**GLOBAL**  Variables & Mixins available to all of our .scss files

**THIRD PARTY** Normalize, Grids, Etc. as well as overrides for those 3rd party bits

**BASE** Unclassed HTML Elements, @font-face, Typography

**OBJECTS** Objects and abstractions. Simple patterns that can be re-used and extended

**COMPONENTS** Full components constructed from objects and their extensions

**PAGES** Per Page Layout Tweaks. Classes unique to specific pages.

**UTILITIES** Single purpose classes with a high specificity

####2. Transom Grid Library

We use bower to import our custom TransomGrid library. 
Read the docs at [github.com/transomdesign/TransomGrid](https://github.com/transomdesign/TransomGrid)

#### 3. Pre-made .scss files

There are a number of .scss files included with this starter. While none of them are "required" they are there to help start your projects in a fast and organized manner. Review `application.scss` to see all of the included files and learn about our organization methods.

##4. Javascript Starter Files

This starter doesn't contain much helper code, but it does define a directory structure for organizing your files. 

3rd party JavaScript libraries are installed with `npm install jquery --save` and compiled with Browserify's `require('jquery')` function. Your project modules live in the `_assets/javascripts`. 

Check out `_assets/javascripts/modules/SampleRevealingModule.js` for an example module. 

**Tips:**
	
- Partials are great
- The [module pattern](http://addyosmani.com/resources/essentialjsdesignpatterns/book/#revealingmodulepatternjavascript) helps us avoid naming conflicts. 
- [FastClick](http://ftlabs.github.io/fastclick/) makes mobile interactions feel quick ( already included ) 

##5. Deployments

We deploy our projects via RSYNC. 

Our pre-defined commands are tailored for Craft CMS projcets. However, they can be easliy edited to fit your project needs. Check out `grunt/rsync.js` to inspect the nitty gritty of these rsync commands.

These commands can be configured from `Grunt.rsync.craft.json`. From here you can define your ssh users, the root path of your server and public files.

####Dev Push/Pull Commands

**Note:** You can review these commands in `grunt/aliases.yaml`

####`grunt initDevServer`

Use this command if you are pushing up to a Transom server for the first time. It cleans out the public directory and uploads all of the CMS & project files. 

####`grunt pushDev`

Builds the project an dpushes your template, config, plugins & assets. 

**Note:** This command doesn't push uploads. We don't want to interfere with anything that clients might be uploading. 

**Note:** These RSYNC commands delete the dev server files before uploading. Files are often renamed or removed during development, deleting ensures that we don't have any unused files lying around. 

#### `grunt pushDevFast`

Skips the build process and plugin upload. 

####`grunt pullDev`
Pulls uploads from dev server

----------------

###Production Push/Pull Commands

####`grunt initProdServer`

####`grunt pushProd`

####`grunt pushProdFast`

####`grunt pullProd`
