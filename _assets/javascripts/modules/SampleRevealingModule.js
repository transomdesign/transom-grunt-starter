/*
 * Revealing Module Sample
 * http://addyosmani.com/resources/essentialjsdesignpatterns/book/#revealingmodulepatternjavascript
 *
 * Overview:
 * A module Include both public/private methods and
 * variables inside a single object, thus shielding
 * particular partsfrom the global scope. What this
 * results in is a reduction in the likelihood of
 * our function names conflicting with other
 * functions defined in additional scripts on the page.
 */

var $ = require('jquery');

var SampleRevealingModule = function(){

  var $body = $('body'),
      privateVar = "Ben Cherry",
      publicVar = "Hey there!";

  /**
   * publicInit()
   *
   * More often than not, we just want to init
   * our module and let it do it's own thing.
   */

  function publicInit() {
    // check and set the modules DOM Element
    // add listeneres

    // Check to see if body element is present, if not, kill init function
    if( $body.length < 1 ) return false;

    $(window).on('resize', function(){
      console.log('myRevealingModule window.resize');
    });

  }

  function privateFunction() {
    console.log( "Name:" + privateVar );
  }

  function publicSetName( strName ) {
    privateVar = strName;
  }

  function publicGetName() {
    privateFunction();
  }


  // Reveal public pointers to
  // private functions and properties

  return {
    init: publicInit,
    setName: publicSetName,
    greeting: publicVar,
    getName: publicGetName
  };
};

module.exports = SampleRevealingModule();
