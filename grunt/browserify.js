module.exports = {
  options: {
    debug: true,
    // transform: ['reactify'],
    // extensions: ['.jsx'],
    plugin: [
      [
        'remapify', [{
            cwd: './_assets/javascripts',
            src: '**/*.js',  // glob for the files to remap
            expose: 'app', // this will expose `__dirname + /client/views/home.js` as `views/home.js`
            // defaults to process.cwd()
          }
        ]
      ]
    ]
  },
  dev: {
    // options: {
    //   alias: ['react:']  // Make React available externally for dev tools
    // },
    src: ['<%= globalConfig.browserify_src %>'],
    dest: '<%= globalConfig.browserify_dest %>'
  },
  // production: {
  //   options: {
  //     debug: false
  //   },
  //   src: '<%= browserify.dev.src %>',
  //   dest: 'build/bundle.js'
  // }
}