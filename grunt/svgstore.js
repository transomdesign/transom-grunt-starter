module.exports = {
  options: {
    includedemo: true,
    prefix: '', // This will prefix each ID
    cleanup: true,
    svg: { // will add and overide the the default xmlns="http://www.w3.org/2000/svg" attribute to the resulting SVG
      viewBox : '0 0 100 100',
      xmlns: 'http://www.w3.org/2000/svg',
      style: 'display: none'
    }
  },
  dist: {
    files: {
      '<%= globalConfig.dest %>/icons/icons.svg': ['<%= globalConfig.src %>/svg-icons/*.svg'],
    }
  }
};