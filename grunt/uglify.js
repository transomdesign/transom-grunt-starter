module.exports = {
  options: {
    compress: {
      // drop_console: true // Remove console.log from minified JS
    }
  },
  dist: {
    src: '<%= globalConfig.dest %>/<%= globalConfig.dest_javascripts %>/<%= globalConfig.dest_javascripts_filename %>.js',
    dest: '<%= globalConfig.dest %>/<%= globalConfig.dest_javascripts %>/<%= globalConfig.dest_javascripts_filename %>.min.js'
  }
};
