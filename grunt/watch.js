module.exports = {
  options: {
    livereload: true
  },
  js: {
    files: ['<%= globalConfig.src %>/javascripts/**/*.js'],
    tasks: ['buildJS']
  },
  sass: {
    files: ['<%= globalConfig.src %>/sass/**/*.scss'],
    tasks: ['buildSASS']
  },
  svg: {
    files: ['<%= globalConfig.src %>/svg-icons/*.svg'],
    tasks: ['svgstore']
  },
  images: {
    files: ['<%= globalConfig.src %>/<%= globalConfig.dest_images %>/**/*.{png,jpg,gif,svg,ico}'],
    tasks: ['clean:images', 'imagemin']
  },
  fonts: {
    files: ['<%= globalConfig.src %>/fonts/**/*'],
    tasks: ['clean:fonts', 'copy:fonts']
  },
  // html: {
  //   files: ['<%= rsync.craft %>/<%= rsync.templates %>/**/*.html']
  // }
};